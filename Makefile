.DEFAULT_GOAL := all

all:

config:
	git config -l

init:
	git init
	git remote add origin git@gitlab.com:gpdowning/cs104c.git
	git add README.md
	git commit -m 'first commit'
	git push -u origin master

pull:
	git pull
	git status

push:
	git add .gitignore
	git add 00-introduction
	git add 01-prefix-sums
	git add 02-shortest-path-i
	git add 03-shortest-path-ii
	git add 04-binary-search
	git add 05-greedy
	git add 06-dfs-backtracking
	git add 07-dynamic-programming-i
	git add 08-number-theory
	git add 09-dynamic-programming-ii
	git add 10-union-find
	git add 11-dynamic-programming-iii
	git add 12-string
	git add 13-segment-trees
	git add 14-max-flow
	git add 15-geometry
	git add Makefile
	git add README.md
	git commit -m "another commit"
	git push
	git status

status:
	git branch
	git remote -v
	git status
